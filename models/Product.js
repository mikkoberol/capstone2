const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
 	name: { 
 		type: String, 
 		required: true 
 	},
	description: { 
		type: String, 
		required: true 
	},
	price: { 
		type: Number, 
		required: true 
	},
	// stock: {
	//     type: Number,
	//     required: [true, "Stock is required"]
	// },
	isActive: { 
		type: Boolean, 
	default: true 
	},
	createdOn: { 
		type: Date, 
	default: new Date
	},
	userOrders: [{
		userId: {
			type: mongoose.Schema.Types.ObjectId,
			required: [true, "UserId is required"]
		},
		orderId: {
			type: String,
		}
	}]
});

module.exports = mongoose.model('Product', productSchema);


const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product")

module.exports.registerUser = (reqBody) =>{

	let newUser = new User({
		email:reqBody.email,
		password: bcrypt.hashSync(reqBody.password,10)
	})

	return newUser.save().then((user,error)=>{

		if(error){
			return false;
		}else{
			return true
		}

	})
	.catch(err=>err)
}


module.exports.checkEmailExists = (reqbody) =>{

	return User.find({email:reqbody.email}).then(result=>{


		if(result.length > 0){
			return "Duplicate email found";
		}

		else{
			return "Email is available";
		}
	})
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result==null){
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect){
				return{access: auth.createAccessToken(result)}
			} else {
				return false;
			};
		};
	});
};

module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;
	});
};

module.exports.createOrder = async (data) => {
  try {

    let user = await User.findById(data.userId);
    if (user.isAdmin) {
      return "Admin access cannot place an order.";
    } else {

      let isUserUpdated = await User.findById(data.userId);
      let isProductUpdated = await Product.findById(data.productId);


      if (!isProductUpdated) {
        throw new Error("Product not found.");
      }

      let quantity = isUserUpdated.orderedProduct.length + 1;
      let totalAmount = isProductUpdated.price * quantity;

      isUserUpdated.orderedProduct.push({
      	products: [
      	{
	        productId: data.productId,
	        productName: isProductUpdated.name,
	        quantity: quantity
	       }
	      ],
	      totalAmount: totalAmount

      });

      await isUserUpdated.save();

      isProductUpdated.userOrders.push({
        userId: data.userId
      });
      await isProductUpdated.save();

      return true;
    }
  } catch (error) {
    console.error(error);
    throw error; 
  }
};


const Product = require("../models/Product");

module.exports.addProduct = (data) => {

if (data.isAdmin) {

let newProduct = new Product({
    name : data.product.name,
    description : data.product.description,
    price : data.product.price
    });
return newProduct.save().then((product, error) => {
    if (error) {
        return false;
        } else {
            return true;
        };
    });


} else {
    return false;
    };

};

module.exports.getAllProducts = (req,res)=>{
    return Product.find({}).then(result=>{
        return result
    })
}

module.exports.getAllActiveProducts = (req,res)=>{
    return Product.find({isActive:true}).then(result=>{
        return result
    })
}

module.exports.getSingleProduct = (reqParams) => {
    return Product.findById(reqParams.productId).then(result => {
        return result;
    });
};

module.exports.updateProduct = (reqParams, reqBody) => {
    let updatedProduct = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    };

    return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
        if (error) {
            return false;
        } else {
            return true;
        };
    });
};

module.exports.archiveProduct = (reqParams)=>{

    let archive = {
        isActive: false
    }

    return Product.findByIdAndUpdate(reqParams.productId, archive).then((product, error)=>{
        if(error){
            return false;
        }else{
            return true;
        }
    })
}

module.exports.activateProduct = (reqParams)=>{

    let activate = {
        isActive: true
    }

    return Product.findByIdAndUpdate(reqParams.productId, activate).then((product, error)=>{
        if(error){
            return false;
        }else{
            return true;
        }
    })
}

module.exports.deleteProduct = (productId) => {
  return Product.findByIdAndDelete(productId)
    .then((product) => {
      if (product) {
        return true;
      } else {
        return false;
      }
    })
    .catch((error) => {
      console.error('Error deleting product:', error);
      throw error;
    });
}


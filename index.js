const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/user")
const productRoutes = require("./routes/product")

const app = express();
const PORT = 4000;

app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

	mongoose.connect("mongodb+srv://mikkob297:rhuQMMGjGvXrVR9e@batch-297.1eei7q9.mongodb.net/capstone2?retryWrites=true&w=majority",
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	)

	let db = mongoose.connection;
	db.on('error', console.error.bind(console,'Connection error'));
	db.once('open', () => console.log('Connected to MongoDB Atlas.'));

	app.use("/users", userRoutes);
	app.use("/product", productRoutes);


if(require.main === module){
	app.listen(process.env.PORT || 4000, () =>{
  		console.log(`Server is running on port ${process.env.PORT || 4000}`);
	})
};


module.exports = {app,mongoose};
const express = require("express");
const userController = require("../controllers/user");
const auth = require("../auth");
const { verify, verifyAdmin } = auth;

const router = express.Router();

router.post("/register",(req,res)=>{
    userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController));
});

router.post("/checkEmail",(req,res)=>{
    userController.checkEmailExists(req.body).then(resultFromController=>res.send(resultFromController))
});

router.post("/login",(req,res)=>{
    userController.loginUser(req.body).then(resultFromController=>res.send(resultFromController))
});

router.get("/details", auth.verify, (req,res) => {
    const userData = auth.decode(req.headers.authorization)
    userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));
});

router.post("/createOrder", auth.verify, (req, res) => {
  const data = {
    userId: auth.decode(req.headers.authorization).id,
    productId:req.body.productId
  };

  userController.createOrder(data)
    .then(resultFromController => {
      res.send(resultFromController);
    })
    .catch(error => {
      console.error(error);
      res.status(500).send("An error occurred while creating the order.");
    });
});



module.exports = router;

const express = require("express");
const productController = require("../controllers/product");

const router = express.Router();
const auth = require("../auth");

router.post("/create", auth.verify, auth.verifyAdmin, (req, res) => {
  const data = {
    product: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  };
  productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});

router.get("/all", auth.verify, auth.verifyAdmin, (req, res) => {
  productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});

router.get("/active", (req, res) => {
  productController.getAllActiveProducts().then(resultFromController => res.send(resultFromController));
});

router.get("/:productId", (req, res) => {
  productController.getSingleProduct(req.params).then(resultFromController => res.send(resultFromController));
});

router.put("/:productId", auth.verify, auth.verifyAdmin, (req, res) => {
  productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

router.patch("/:productId/archive", auth.verify, auth.verifyAdmin, (req, res) => {
  productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController));
});

router.patch("/:productId/activate", auth.verify, auth.verifyAdmin, (req, res) => {
  productController.activateProduct(req.params).then(resultFromController => res.send(resultFromController));
});

router.delete("/:productId", auth.verify, auth.verifyAdmin, (req, res) => {
  productController.deleteProduct(req.params.productId)
    .then(resultFromController => {
      if (resultFromController) {
        res.status(200).json({ message: "Product deleted successfully." });
      } else {
        res.status(404).json({ error: "Product not found." });
      }
    })
    .catch(error => {
      res.status(500).json({ error: "Error deleting product.", details: error });
    });
});


module.exports = router;